var rangeInput = $('input[type="range"]'),
  rangeCircle = $('.range');

var rangeCredit = rangeInput.attr('value');
rangeCircle.attr('data-percentage', rangeCredit);

function changeRange(changedValue) {
  rangeCircle.attr('data-percentage', changedValue);
}