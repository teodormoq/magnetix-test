Teodor Moquist Boilerplate
==========================

Personalized boilerplate based on gulp...

* First of all download or clone this repo

You'll need to have Gulp installed. If you already have Gulp installed, you can skip the following line.
To install gulp simply run the following line in your terminal:
* `npm install -g gulp`

* Run `install.cmd` to install dev dependencies. On mac fire up terminal and type `npm install && bower install` use sudo if needed.

During development mode, run the default task so you'll have watchers and browser sync.
* Run `start-server.cmd` to set fire on this boilerplate!

Features included
==========================

 * [Bower](https://bower.io/ "Bower's Homepage")
 * [BrowserSync](https://www.browsersync.io "BrowserSync's Homepage")
 * [CSS - Preprocessor (SASS)](http://sass-lang.com "SASS's Homepage")
 * [CSS - Authoring Framework (Compass)](http://compass-style.org/ "Compass's Homepage")
 * [CSS - Minify](https://www.npmjs.com/package/gulp-clean-css "Clean-css's NPM")
 * [CSS - Shorthand](https://www.npmjs.com/package/gulp-shorthand "Shorthand's NPM")
 * [JS - Minify](https://www.npmjs.com/package/gulp-uglify "UglifyJS's NPM")
 * [Jshint](https://github.com/spalger/gulp-jshint "Jshint's Github")
 * [Concat](https://www.npmjs.com/package/gulp-concat "Concat's NPM")

Update dependencies
==========================

In order to ensure that all dependencies are updated use:

https://www.npmjs.org/package/npm-check-updates

$ `npm install -g npm-check-updates`

$ `npm-check-updates -u`

$ `npm install && bower install`

This will automatically adjusts a package.json with the latest version of all dependencies!

Contributing
==========================

Contributions are welcome!
 * [Linkedin](https://www.linkedin.com/in/teodor-moquist-31393498/ "Teodor Moquist's Linkedin")
 * [Homepage](http://teomedia.dk/ "Teodor Moquist's Homepage")
