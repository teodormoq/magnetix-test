// Include Our Plugins
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var compass = require('gulp-compass'),
    path = require('path');
var shorthand = require('gulp-shorthand');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var mainBowerFiles = require('main-bower-files');
var browserSync = require('browser-sync').create();

// Direct to your app folder
var dir = 'app/';

// Lint Task
gulp.task('lint', function() {
  return gulp.src(dir + 'scripts/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Compile Our bower components
gulp.task('bower', function() {
  return gulp.src(mainBowerFiles(), { base: '/bower_components' })
    .pipe(concat('vendor.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(dir + '.tmp/js'));
});

// Compile Our Compass and Sass
gulp.task('styles', function() {
  return gulp.src([dir + 'styles/**/*.scss'])
    .pipe(compass({
      css: dir + 'styles',
      sass: dir + 'styles',
      image: dir + 'images'
    }))
    .on('error', function(error) {
      console.log(error);
      this.emit('end');
    })
    .pipe(shorthand())
    .pipe(minifyCSS())
    .pipe(gulp.dest(dir + '.tmp/css'))
    .pipe(browserSync.stream());
})

// Concatenate & Minify JS
gulp.task('scripts', function() {
  return gulp.src(dir + 'scripts/**/*.js')
    .pipe(concat('main.js'))
    .pipe(rename('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(dir + '.tmp/js'));
});

// Task that ensures the `scripts` task is complete before reloading browser
gulp.task('js-watch', ['scripts', 'lint'], function (done) {
    browserSync.reload();
    done();
});

// Serve Task
gulp.task('serve', ['lint', 'styles', 'js-watch', 'bower'], function() {

    browserSync.init({
        server: './app'
    });

    gulp.watch(dir + 'scripts/**/*.js', ['lint', 'js-watch']);
    gulp.watch(dir + 'styles/**/*.scss', ['styles']);
    gulp.watch(dir + '**/*.html').on('change', browserSync.reload);
});

// Default Task
gulp.task('default', ['serve']);